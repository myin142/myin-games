#!/bin/sh

HEROKU_API="$1"
HEROKU_APP=sakkaku-socket-app
HEROKU_FOLDER=heroku

if [ ! -d dist/apps/socket-server ]; then
    echo "No socket-server files to deploy"
    exit
fi

if [ -z "$HEROKU_API" ]; then
    heroku git:remote -a $HEROKU_APP
else
    git remote add heroku https://heroku:$HEROKU_API@git.heroku.com/$HEROKU_APP.git
fi

git add -f dist/apps/socket-server
git commit -m 'Heroku Deploy'
git push heroku `git subtree split --prefix dist/apps/socket-server HEAD`:master --force

if [ -z "$HEROKU_API" ]; then
    git reset HEAD~1
fi