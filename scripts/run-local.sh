#!/bin/sh

DYNAMO=http://localhost:8000
# AWS_FOLDER=apps/aws
NEW=false

while [ -n "$1" ]; do
    case "$1" in
        --new) NEW=true;;
        --build) BUILD=true;;
        *) STACK="$1";;
    esac

    shift
done

if [ "$NEW" = true ]; then
    docker-compose down
fi
docker-compose up --remove-orphans -d

if [ "$NEW" = true ]; then
    aws dynamodb create-table \
        --table-name WebSocketRoom \
        --attribute-definitions AttributeName=room,AttributeType=S AttributeName=user,AttributeType=S \
        --key-schema AttributeName=room,KeyType=HASH AttributeName=user,KeyType=SORT \
        --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
        --endpoint-url "$DYNAMO" > /dev/null 2>&1
    echo "Created WebSocketRoom Table"
fi

# cd $AWS_FOLDER
# cdk synth --no-staging | sed -ne '/Resources:/,$p' > template.yml
# sam local start-api --docker-network host