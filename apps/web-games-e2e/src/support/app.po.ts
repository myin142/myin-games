export const getGreeting = () => cy.get('h1');

export const loginRoom = () => cy.get('[data-cy-login-room]');
export const loginUser = () => cy.get('[data-cy-login-user]');
export const loginJoin = () => cy.get('[data-cy-login-join]');

export const chatInput = () => cy.get('[data-cy-chat-input] input');
export const chatSend = () => cy.get('[data-cy-chat-send]');
export const chatMessages = () => cy.get('[data-cy-chat-messages]');