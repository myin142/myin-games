import { v4 as uuid } from 'uuid';
import { chatInput, chatSend, chatMessages } from '../support/app.po';
import { SocketClient } from '@myin-games/socket-client';
import { GamesClient } from '@myin-games/games-client';

describe('web-games', () => {

    let room: string;
    const socketUrl = 'ws://localhost:4000';

    const createClient = async (user: string) => {
        const client = new GamesClient(new SocketClient(socketUrl));
        client.joinRoom({ room, user });
        return client;
    }

    beforeEach(() => {
        cy.visit('/');
        room = uuid();
    });

    it('lobby chat', () => {
        cy.login(room, 'Host');

        // Show own message
        chatInput().type('Hello{enter}');
        chatInput().should('have.value', '');
        chatMessages().as('messages').should('contain', 'Host: Hello').then(async () => {
            // User joined
            const client = await createClient('User');
            chatMessages().should('contain', 'User has joined').then(() => {
                // User message
                client.sendMessage('Hello Back');
                chatMessages().should('contain', 'User: Hello Back').then(() => {
                    // User left
                    client.leave();
                    chatMessages().should('contain', 'User has left');
                });
            });
        })
    });
});
