import * as cdk from '@aws-cdk/core';
import { User } from '@aws-cdk/aws-iam';
import { Table, AttributeType } from '@aws-cdk/aws-dynamodb';
import { webSocketRoomTable } from '../../../../libs/socket-shared/src';

export class AppStack extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        // The code that defines your stack goes here
        const webSocketTable = new Table(this, 'WebSocketTable', {
            tableName: webSocketRoomTable,
            partitionKey: {
                name: 'room',
                type: AttributeType.STRING
            },
            sortKey: {
                name: 'user',
                type: AttributeType.STRING
            },
        })

        const user = new User(this, 'HerokuUser');
        webSocketTable.grantReadWriteData(user);
    }
}
