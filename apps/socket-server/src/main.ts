import { SocketServer } from './app/socket-server';

const port = parseInt(process.env.PORT) || 4000;
new SocketServer(port);