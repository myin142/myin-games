import * as WebSocket from 'ws';
import { SocketConnection, webSocketRoomTable } from '@myin-games/socket-shared';
import { DynamoDB } from 'aws-sdk';
import { environment } from '../environments/environment';
import { toAWSAttributeMap, toSimpleAWSKeyConditionExpressions, fromAWSAttributeMap, } from '@myin/utils';

const defaultDynamo = new DynamoDB({ region: 'eu-central-1', endpoint: environment.dynamoEndpoint });

export class SocketRoomManager {

    private sockets: { [u: string]: WebSocket } = {};

    constructor(private dynamo: DynamoDB = defaultDynamo) { }

    async userJoined({ room, user }: SocketConnection, socket: WebSocket) {
        await this.dynamo.putItem({
            TableName: webSocketRoomTable,
            Item: toAWSAttributeMap({ room, user }),
        }).promise();

        console.log(`User ${user} joined ${room}`);
        this.sockets[user] = socket;
    }

    async userLeft({ room, user }: SocketConnection) {
        await this.dynamo.deleteItem({
            TableName: webSocketRoomTable,
            Key: toAWSAttributeMap({ room, user }),
        }).promise();

        console.log(`User ${user} left ${room}`);
        const socket = this.sockets[user];
        delete this.sockets[user];
        if (socket && socket.readyState === WebSocket.OPEN) {
            await new Promise(resolve => {
                socket.onclose = resolve;
                socket.close();
            });
        }
    }

    async otherUserSocketsInRoom({ user, room }: SocketConnection): Promise<WebSocket[]> {
        const response = await this.dynamo.query({
            TableName: webSocketRoomTable,
            ...toSimpleAWSKeyConditionExpressions({ room }),
        }).promise();

        return response.Items
            .map(i => fromAWSAttributeMap(i) as SocketRoom)
            .filter(i => i.user !== user)
            .map(i => this.sockets[i.user])
            .filter(i => !!i);
    }

    async findUserInRoom({ room, user }: SocketConnection): Promise<SocketRoom> {
        const response = await this.dynamo.getItem({
            TableName: webSocketRoomTable,
            Key: toAWSAttributeMap({ room, user }),
        }).promise();

        return fromAWSAttributeMap(response.Item);
    }

    async findUserSocket(conn: SocketConnection): Promise<WebSocket> {
        const user = await this.findUserInRoom(conn);
        return this.sockets[user.user];
    }

    /** DO NOT call this method in production, only used in tests */
    async clear() {
        const response = await this.dynamo.scan({ TableName: webSocketRoomTable }).promise();
        await Promise.all(response.Items
            .map(i => fromAWSAttributeMap(i) as SocketRoom)
            .map(i => this.userLeft({ room: i.room, user: i.user })));
    }

}

export interface SocketRoom {
    room: string;
    user: string;
    data?: string;
}
