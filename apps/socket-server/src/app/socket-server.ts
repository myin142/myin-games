import * as WebSocket from 'ws';
import { SocketConnection, SocketMessageType, SocketMessage } from '@myin-games/socket-shared';
import { SocketRoomManager } from './socket-rooms';

export class SocketServer {

    private server: WebSocket.Server;
    private roomManager: SocketRoomManager;
    private started = false;

    private sendUserJoined(socket: WebSocket, user: string): void {
        socket.send(this.message(SocketMessageType.UserJoined, user));
    }

    private sendUserLeft(socket: WebSocket, user: string): void {
        socket.send(this.message(SocketMessageType.UserLeft, user));
    }

    private sendUserMessage(socket: WebSocket, data: any): void {
        socket.send(this.message(SocketMessageType.Message, data));
    }

    private async eachUser(conn: SocketConnection, handler: (s: WebSocket) => void) {
        const sockets = await this.roomManager.otherUserSocketsInRoom(conn);
        sockets.map(s => handler(s));
    }

    constructor(port: number) {
        this.roomManager = new SocketRoomManager();
        this.server = new WebSocket.Server({ port }, () => {
            this.started = true;
            console.log(`Websocket Server started at port ${port}`);
        });

        this.server.on('connection', async (socket, { url }) => {
            const conn = this.parseConnection(url);
            this.roomManager.userJoined(conn, socket).then(() => {
                this.eachUser(conn, s => this.sendUserJoined(s, conn.user));
            });

            socket.onclose = async () => {
                await this.roomManager.userLeft(conn);
                await this.eachUser(conn, s => this.sendUserLeft(s, conn.user));
            }

            socket.onmessage = async ev => {
                const msg = JSON.parse(ev.data as string) as SocketMessage;
                if (msg.user) {
                    const userSocket = await this.roomManager.findUserSocket({ ...conn, user: msg.user });
                    userSocket.send(ev.data);
                } else {
                    await this.eachUser(conn, s => s.send(ev.data));
                }
            }
        });
    }

    private parseConnection(url: string): SocketConnection {
        const params = new URLSearchParams(url.split('?')[1]);
        return {
            room: params.get('room'),
            user: params.get('user'),
        };
    }

    private message(type: SocketMessageType, data: any): string {
        const msg: SocketMessage = { type, data };
        return JSON.stringify(msg);
    }

    isStarted(): boolean {
        return this.started;
    }

    async clearConnections() {
        await this.roomManager.clear();
    }

}
