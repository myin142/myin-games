import { SocketServer } from './socket-server';
import { SocketConnection, SocketMessageType, SocketMessage } from '@myin-games/socket-shared';
import { SocketClient } from '@myin-games/socket-client';
import { filter } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';

jest.setTimeout(10 * 1000);

describe('Socket Server', () => {

    const port = 9090;
    const url = `ws://localhost:${port}`;

    let room: string;
    let server: SocketServer;
    let messages: { [u: string]: SocketMessage[] } = {};

    const collectMessages = (client: SocketClient) => {
        const user = client.getConnection().user;
        client.onMessage().subscribe(msg => {
            if (!messages[user]) messages[user] = [];
            messages[user].push(msg);
        });
    }

    const getMessages = (client: SocketClient, msg?: Partial<SocketMessage>): SocketMessage[] => {
        let allMessages = messages[client.getConnection().user] || [];
        if (msg) {
            allMessages = allMessages.filter(m => partialMessageMatch(m, msg));
        }
        return allMessages;
    }

    const wait = async (timeInMillis) => new Promise(resolve => setTimeout(resolve, timeInMillis));
    const waitUntil = async (condition) => {
        while (!condition()) await new Promise(resolve => setTimeout(resolve, 500));
    };
    const connectClient = async (conn: SocketConnection) => {
        const client = new SocketClient(url);
        await client.connect(conn);
        return client;
    }

    const expectMessage = async (client: SocketClient, msg: Partial<SocketMessage>): Promise<SocketMessage> => {
        return new Promise(resolve => {
            client.onMessage()
                .pipe(filter(m => partialMessageMatch(m, msg)))
                .subscribe(resolve);
        });
    }

    const partialMessageMatch = (msg: SocketMessage, m: Partial<SocketMessage>): boolean => {
        return (!m.type || msg.type === m.type) && (!m.data || msg.data === m.data);
    }

    beforeAll(async () => {
        server = new SocketServer(port);
        await waitUntil(() => server.isStarted());
    });

    afterAll(async () => {
        await server.clearConnections();
    });

    beforeEach(() => {
        messages = {};
        room = uuid();
    });

    it('join and notify others', async done => {
        const client = await connectClient({ user: 'Client', room });
        collectMessages(client);

        expectMessage(client, { type: SocketMessageType.UserJoined, data: 'User' })
            .then(() => {
                expect(getMessages(client, { type: SocketMessageType.UserJoined, data: 'Client' })).toEqual([]);
                done();
            });

        await connectClient({ user: 'User', room });
    });

    it('leave and notify others', async done => {
        const client = await connectClient({ user: 'Client', room });
        const user = await connectClient({ user: 'User', room });
        collectMessages(user);

        expectMessage(user, { type: SocketMessageType.UserLeft, data: 'Client' })
            .then(() => {
                expect(getMessages(user, { type: SocketMessageType.UserLeft, data: 'User' })).toEqual([]);
                done();
            });

        await client.disconnect();
    });

    it('send message to other users in room', async done => {
        const client = await connectClient({ user: 'Client', room });
        collectMessages(client);

        const user1 = await connectClient({ user: 'User 1', room });
        const user2 = await connectClient({ user: 'User 2', room });

        const data: SocketMessage = {
            type: SocketMessageType.Message,
            data: 'Data',
        };

        Promise.all([expectMessage(user1, data), expectMessage(user2, data)])
            .then(() => {
                expect(getMessages(client, { type: SocketMessageType.Message })).toEqual([]);
                done();
            });
        client.send(data);
    });

    it('send message to single user in room', async done => {
        const client = await connectClient({ user: 'Client', room });

        const user1 = await connectClient({ user: 'User 1', room });
        const user2 = await connectClient({ user: 'User 2', room });
        collectMessages(user2);

        const data: SocketMessage = {
            type: SocketMessageType.Message,
            data: 'Data',
            user: 'User 1',
        };

        expectMessage(user1, data).then(async () => {
            // Wait to make sure it really did not get the message
            await wait(500);
            expect(getMessages(user2, { type: SocketMessageType.Message })).toEqual([]);
            done();
        });
        client.send(data);
    });

});