import { SocketClient } from '@myin-games/socket-client';
import { GamesClient } from '@myin-games/games-client';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './app/app';
import { environment } from './environments/environment';

const socketClient = new SocketClient(environment.socketUrl);
const gamesClient = new GamesClient(socketClient);
ReactDOM.render(<App gamesClient={gamesClient} />, document.getElementById('root'));
