export const environment = {
    production: true,
    baseHref: '/myin-games',
    socketUrl: 'wss://sakkaku-socket-app.herokuapp.com',
};
