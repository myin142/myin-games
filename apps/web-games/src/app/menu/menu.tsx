import React from 'react';

import { useHistory } from 'react-router-dom';
import { AppBar, Toolbar, Typography, Button } from '@material-ui/core';

import './menu.scss';

export interface MenuProps {
  loggedIn: boolean;
  logoutHandler: () => Promise<void>;
}

// Use functional component to access useHistory
export const Menu = (props: MenuProps) => {
  const history = useHistory();
  const title = props.loggedIn ? 'Lobby' : 'Join Room';

  const logout = async () => {
    await props.logoutHandler();
    history.push('/');
  };

  return (
    <AppBar position="static">
      <Toolbar className="menu" variant="dense">
        <Typography variant="h6" className="title">
          {title}
        </Typography>
        {props.loggedIn && (
          <Button color="inherit" onClick={logout}>
            Logout
          </Button>
        )}
      </Toolbar>
    </AppBar>
  );
};
