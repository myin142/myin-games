import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Menu } from './menu/menu';
import { Login } from '@myin-games/login';
import { Lobby } from '@myin-games/lobby';

import { environment } from '../environments/environment';
import { GamesClient } from '@myin-games/games-client';
import { SocketConnection } from '@myin-games/socket-shared';

function GuardedRoute(condition: () => boolean, elsePath: string) {
  return function ({ component: Component, ...rest }) {
    return (
      <Route
        {...rest}
        render={(props) => (condition() ? <Component {...props} /> : <Redirect to={elsePath} />)}
      />
    );
  };
}

export interface AppProps {
  gamesClient: GamesClient;
}

export interface AppState {
  loggedIn: boolean;
}

export default class App extends React.Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props);

    this.state = {
      loggedIn: false,
    };
  }

  async handleLogin(conn: SocketConnection) {
    await this.props.gamesClient.joinRoom(conn);
    this.setState({
      loggedIn: this.props.gamesClient.isInRoom(),
    });
  }

  async handleLogout() {
    await this.props.gamesClient.leave();
    this.setState({
      loggedIn: this.props.gamesClient.isInRoom(),
    });
  }

  render() {
    const AuthRoute = GuardedRoute(() => this.state.loggedIn, '/login');
    const LoginRoute = GuardedRoute(() => !this.state.loggedIn, '/');

    return (
      <BrowserRouter basename={environment.baseHref}>
        <Menu loggedIn={this.state.loggedIn} logoutHandler={this.handleLogout.bind(this)} />
        <Switch>
          <Route exact path="/">
            <Redirect to="/lobby"></Redirect>
          </Route>
          <LoginRoute
            path="/login"
            component={(props) => <Login loginHandler={this.handleLogin.bind(this)} {...props} />}
          ></LoginRoute>
          <AuthRoute
            path="/lobby"
            component={() => <Lobby gamesClient={this.props.gamesClient} />}
          ></AuthRoute>
        </Switch>
      </BrowserRouter>
    );
  }
}
