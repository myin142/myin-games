import React from 'react';
import { render } from '@testing-library/react';
import { mock, MockProxy } from 'jest-mock-extended';
import { SocketClient } from '@myin-games/socket-client';

import App from './app';
import { GamesClient } from '@myin-games/games-client';

jest.mock('@myin-games/login', () => ({ Login: () => 'Login' }));
jest.mock('@myin-games/lobby', () => ({ Lobby: () => 'Lobby' }));

describe('App', () => {
  let gamesClient: MockProxy<GamesClient> & GamesClient;

  beforeEach(() => {
    gamesClient = mock();
  });

  it('should render successfully', () => {
    const { baseElement } = render(<App gamesClient={gamesClient} />);
    expect(baseElement).toBeTruthy();
  });

  it('should show login if user not connected', () => {
    gamesClient.isInRoom.mockReturnValue(false);
    const { baseElement } = render(<App gamesClient={gamesClient} />);
    expect(baseElement.textContent).toEqual('Login');
  });

  it('should show lobby if user is defined', () => {
    gamesClient.isInRoom.mockReturnValue(true);
    const { baseElement } = render(<App gamesClient={gamesClient} />);
    expect(baseElement.textContent).toEqual('Lobby');
  });
});
