import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Box, Typography } from '@material-ui/core';

import './lobby.scss';
import { ChatMessage, GamesClient } from '@myin-games/games-client';
import { Chat } from './chat/chat';

export interface LobbyProps {
  gamesClient: GamesClient;
}

export interface LobbyState {
  message: string;
  messages: ChatMessage[];
}

export class Lobby extends React.Component<LobbyProps, LobbyState> {
  constructor(props) {
    super(props);

    this.state = {
      message: '',
      messages: [],
    };
  }

  sendMessage() {
    this.props.gamesClient.sendMessage(this.state.message);
  }

  render() {
    return (
      <Box display="flex" flexGrow="1">
        <Box flexGrow="1">
          <Switch>
            <Route path="/">
              <Typography variant="h6" className="lobby-wait">
                Waiting for host to start a game
              </Typography>
            </Route>
          </Switch>
        </Box>
        <Chat gamesClient={this.props.gamesClient} />
      </Box>
    );
  }
}
