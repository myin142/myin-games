import { ChatMessage } from '@myin-games/games-client';
import React, { Component } from 'react';
import { ListItem, ListItemText, List } from '@material-ui/core';
import './chat-messages.scss';

export interface ChatMessagesProps {
  messages: ChatMessage[];
  user: string;
}

export class ChatMessages extends Component<ChatMessagesProps> {
  private messageClassName(msg: ChatMessage): string {
    return msg.user === this.props.user ? 'own-message' : '';
  }

  render() {
    const userMessage = (msg: ChatMessage) => (
      <span>
        <strong>{msg.user}:</strong> {msg.text}
      </span>
    );
    const statusMessage = (msg: ChatMessage) => <span className="status">{msg.text}</span>;

    const chatMessageItems = this.props.messages.map((msg, i) => (
      <ListItem key={i} data-cy-chat-messages>
        <ListItemText className={this.messageClassName(msg)}>
          {msg.user ? userMessage(msg) : statusMessage(msg)}
        </ListItemText>
      </ListItem>
    ));

    return <List className="messages">{chatMessageItems}</List>;
  }
}
