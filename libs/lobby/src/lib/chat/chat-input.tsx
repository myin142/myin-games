import React from 'react';
import { TextField, Button, Box } from '@material-ui/core';

export interface ChatInputProps {
  sendMessageHandler: (msg: string) => void;
}

export interface ChatInputState {
  message: string;
}

export class ChatInput extends React.Component<ChatInputProps, ChatInputState> {
  constructor(props) {
    super(props);

    this.state = {
      message: '',
    };
  }

  private messageChange({ target }: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ message: target.value });
  }

  private sendMessage() {
    if (this.state.message) {
      this.props.sendMessageHandler(this.state.message);
      this.setState({ message: '' });
    }
  }

  private sendMessageOnEnter({ charCode }: React.KeyboardEvent<HTMLInputElement>) {
    if (charCode === 13) {
      this.sendMessage();
    }
  }

  render() {
    return (
      <Box display="flex">
        <TextField
          value={this.state.message}
          onChange={this.messageChange.bind(this)}
          onKeyPress={this.sendMessageOnEnter.bind(this)}
          data-cy-chat-input
        />
      </Box>
    );
  }
}
