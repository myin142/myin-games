import React, { RefObject } from 'react';
import { Card, CardActions } from '@material-ui/core';
import { ChatMessage, GamesClient, ChatJoin } from '@myin-games/games-client';
import { ChatMessages } from './chat-messages';
import { ChatInput } from './chat-input';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import './chat.scss';

export interface ChatProps {
  gamesClient: GamesClient;
}
export interface ChatState {
  messages: ChatMessage[];
}

export class Chat extends React.Component<ChatProps, ChatState> {
  private onUnmount = new Subject();
  private chatWrapper: RefObject<HTMLElement>;

  constructor(props) {
    super(props);

    this.chatWrapper = React.createRef();
    this.state = {
      messages: [],
    };

    this.props.gamesClient
      .onChatMessage()
      .pipe(takeUntil(this.onUnmount))
      .subscribe((msg) => this.addMessage(msg));

    this.props.gamesClient
      .onChatJoin()
      .pipe(takeUntil(this.onUnmount))
      .subscribe((msg) => this.addJoinStatus(msg));
  }

  private addJoinStatus({ joined, user }: ChatJoin): void {
    const status = joined ? 'joined' : 'left';
    const text = `${user} has ${status}`;
    this.addMessage({ text });
  }

  private addMessage(msg: ChatMessage): void {
    this.setState((state) => ({
      messages: [...state.messages.slice(0, 50), msg],
    }));
  }

  componentWillUnmount() {
    this.onUnmount.next();
  }

  private sendMessage(msg: string) {
    this.props.gamesClient.sendMessage(msg);
    this.addMessage({ text: msg, user: this.props.gamesClient.user });
  }

  private scrollChatBottom() {
    const chatElem = this.chatWrapper.current;
    if (chatElem) {
      chatElem.scrollTop = chatElem.scrollHeight + 100;
    }
  }

  render() {
    this.scrollChatBottom();

    return (
      <Card className="chat" ref={this.chatWrapper}>
        <ChatMessages messages={this.state.messages} user={this.props.gamesClient.user} />
        <CardActions>
          <ChatInput sendMessageHandler={this.sendMessage.bind(this)} />
        </CardActions>
      </Card>
    );
  }
}
