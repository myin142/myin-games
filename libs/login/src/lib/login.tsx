import React from 'react';
import { Box, Button, FormControl, Input, InputLabel } from '@material-ui/core';

import './login.scss';
import { SocketConnection } from '@myin-games/socket-shared';
import { RouteComponentProps } from 'react-router-dom';

export interface LoginProps extends RouteComponentProps {
  loginHandler: (c: SocketConnection) => Promise<void>;
}

export interface LoginState {
  room: string;
  user: string;
}

export class Login extends React.Component<LoginProps, LoginState> {
  constructor(props) {
    super(props);

    this.state = {
      room: '',
      user: '',
    };
  }

  roomChange({ target }: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({ room: target.value });
  }

  userChange({ target }: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({ user: target.value });
  }

  async joinRoom() {
    await this.props.loginHandler({
      ...this.state,
    });
    this.props.history.push('/');
  }

  render() {
    return (
      <Box display="flex" flexDirection="column">
        <FormControl>
          <InputLabel htmlFor="room">Room</InputLabel>
          <Input
            id="room"
            value={this.state.room}
            onChange={this.roomChange.bind(this)}
            data-cy-login-room
          />
        </FormControl>

        <FormControl>
          <InputLabel htmlFor="user">User</InputLabel>
          <Input
            id="user"
            value={this.state.user}
            onChange={this.userChange.bind(this)}
            data-cy-login-user
          />
        </FormControl>

        <Button onClick={this.joinRoom.bind(this)} data-cy-login-join>
          Join
        </Button>
      </Box>
    );
  }
}
