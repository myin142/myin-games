import { SocketConnection, SocketMessage, SocketMessageType } from '@myin-games/socket-shared';
import { Subject, Observable } from 'rxjs';

export class SocketClient {

    private webSocket: WebSocket;
    private message = new Subject<SocketMessage>();
    private connection: SocketConnection;

    constructor(private baseUrl: string) { }

    isConnected(): boolean {
        return this.webSocket && this.webSocket.readyState === WebSocket.OPEN;
    }

    async disconnect(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.webSocket.onerror = () => reject();
            this.webSocket.onclose = () => resolve();
            this.webSocket.close();
        });
    }

    async connect({ room, user }: SocketConnection): Promise<void> {
        this.webSocket = new WebSocket(`${this.baseUrl}?user=${user}&room=${room}`);

        return new Promise((resolve, reject) => {
            this.webSocket.onerror = () => reject();
            this.webSocket.onopen = () => {
                this.webSocket.onmessage = msg => this.message.next(JSON.parse(msg.data));
                this.connection = { room, user };
                resolve();
            }
        });
    }

    getConnection(): SocketConnection {
        return this.connection;
    }

    onMessage(): Observable<SocketMessage> {
        return this.message;
    }

    send(msg: SocketMessage): void {
        this.webSocket.send(JSON.stringify(msg));
    }

}
