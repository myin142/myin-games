export interface GameMessage<T> {
    type: GameMessageType;
    data: T;
}

export enum GameMessageType {
    Chat,
}

export interface ChatMessage {
    user?: string;
    text: string;
}

export interface ChatJoin {
    user: string;
    joined: boolean;
}