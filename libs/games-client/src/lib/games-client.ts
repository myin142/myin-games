import { SocketClient } from '@myin-games/socket-client';
import { SocketConnection, SocketMessageType } from '@myin-games/socket-shared';
import { Observable, Subject } from 'rxjs';
import { GameMessage, GameMessageType, ChatMessage, ChatJoin } from './game-message';

export class GamesClient {

    private chatMessage = new Subject<ChatMessage>();
    private chatJoin = new Subject<ChatJoin>();

    constructor(private socketClient: SocketClient) {
        this.socketClient.onMessage().subscribe(msg => {
            switch (msg.type) {
                case SocketMessageType.Message:
                    this.handleGameMessage(msg.data as GameMessage<any>);
                    break;
                case SocketMessageType.UserJoined:
                    this.chatJoin.next({ joined: true, user: msg.data });
                    break;
                case SocketMessageType.UserLeft:
                    this.chatJoin.next({ joined: false, user: msg.data });
                    break;
            }
        });
    }

    private handleGameMessage({ type, data }: GameMessage<any>) {
        switch (type) {
            case GameMessageType.Chat:
                this.chatMessage.next(data as ChatMessage);
                break;
        }
    }

    get user(): string {
        return this.isInRoom() ? this.socketClient.getConnection().user : null;
    }

    sendMessage(msg: string) {
        const chatMsg: GameMessage<ChatMessage> = {
            type: GameMessageType.Chat,
            data: {
                text: msg,
                user: this.user,
            },
        };

        this.socketClient.send({
            type: SocketMessageType.Message,
            data: chatMsg,
        });
    }

    onChatMessage(): Observable<ChatMessage> {
        return this.chatMessage;
    }

    onChatJoin(): Observable<ChatJoin> {
        return this.chatJoin;
    }

    async joinRoom(conn: SocketConnection) {
        await this.socketClient.connect(conn);
    }

    async leave() {
        await this.socketClient.disconnect();
    }

    isInRoom(): boolean {
        return this.socketClient.isConnected();
    }

}
