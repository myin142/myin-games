export interface SocketConnection {
    user: string;
    room: string;
}

export enum SocketMessageType {
    UserJoined,
    UserLeft,

    Message,
}

export interface SocketMessage {
    type: SocketMessageType;
    data: any;

    /** used for Message type to specify which user to send to */
    user?: string;
}

export const webSocketRoomTable = 'WebSocketRoom';